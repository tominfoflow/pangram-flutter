import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pangram_flutter/common/routes/router.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pangram And Anagram',
      home: Router.home,
      onGenerateRoute: Router.onGenerateRoute,
    );
  }

}