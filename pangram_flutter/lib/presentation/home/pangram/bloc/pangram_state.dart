part of 'pangram_bloc.dart';

@immutable
abstract class PangramState {}

@immutable
class PangramInitial extends PangramState {}

@immutable
class PangramOfTheDayState extends PangramState{
  final String text;

  PangramOfTheDayState({this.text});
}