part of 'pangram_bloc.dart';

@immutable
abstract class PangramEvent {}

@immutable
class PangramEventInit extends PangramEvent{

}

@immutable
class PangramOfTheDay extends PangramEvent{
}