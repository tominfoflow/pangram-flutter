import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:pangram_flutter/common/model/usecase.dart';
import 'package:pangram_flutter/domain/usecases/pangram_usecase.dart';

part 'pangram_event.dart';
part 'pangram_state.dart';

@injectable
class PangramBloc extends Bloc<PangramEvent, PangramState> {
  final PangramOfTheDayUseCase pangramOfTheDayUseCase;

  PangramBloc({this.pangramOfTheDayUseCase});

  @override
  PangramState get initialState => PangramInitial();

  @override
  Stream<PangramState> mapEventToState(
    PangramEvent event,
  ) async* {
    if(event is PangramEventInit){
      try{
        final pangramOfTheDay = await pangramOfTheDayUseCase(NoPayload());
        yield PangramOfTheDayState(text : pangramOfTheDay);
      }catch(e){
        print(e);
      }
    }    
  }
}
