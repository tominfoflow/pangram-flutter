import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pangram_flutter/presentation/home/pangram/bloc/pangram_bloc.dart';

class PangramPage extends StatefulWidget {
  PangramPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PangramPageState();
}

class _PangramPageState extends State<PangramPage> {
  String _pangramOfTheDay = '';

  @override
  void initState() {
    super.initState();
    BlocProvider.of<PangramBloc>(context).add(PangramEventInit());
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child:
            BlocConsumer<PangramBloc, PangramState>(builder: (context, state) {
      if (state is PangramOfTheDayState) {
        _pangramOfTheDay = state.text;
        return _buildPangramOfTheDay();
      } else {
        return Center(
          child: Center(child: CircularProgressIndicator()),
        );
      }
    }, listener: (context, state) {
      return Center(
        child: Text('loading'),
      );
    }));
  }

  Widget _buildPangramOfTheDay() {
    return SingleChildScrollView(
        child: Padding(
            padding: EdgeInsets.all(15.0), child: Text(_pangramOfTheDay)));
  }
}
