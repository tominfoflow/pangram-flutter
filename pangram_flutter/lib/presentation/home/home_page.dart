import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pangram_flutter/common/routes/router.dart';
import 'package:pangram_flutter/presentation/home/anagram/anagram.dart';
import 'package:pangram_flutter/presentation/home/isogram/isogram.dart';
import 'package:pangram_flutter/presentation/home/isogram/random_isogram.dart';
import 'package:pangram_flutter/presentation/home/pangram/pangram.dart';

class HomePage extends StatefulWidget {
  final HomePageOptions page;

  const HomePage({Key key, this.page}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _title = 'Pangram Of The Day';
  int _selectedPageIndex;

  @override
  void initState() {
    super.initState();
    _selectedPageIndex = widget.page?.index ?? 0;
  }

  final List<Widget> _pages = [PangramPage(), AnagramPage(), IsogramPage(), RandomIsogramPage()];

  final List<String> _titles = [
    'Panagram Of The Day',
    'Anagram',
    'Isogram', 
    'Guess Isogram', 
  ];

  void _onPageIndexChanged(int value){
    setState(() {
      _title = _titles[value];
      _selectedPageIndex = value;
    });
  }

  BottomNavigationBar _buildBottomNavigationBar() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Icons.text_fields), title: Text('Panagram')),
        BottomNavigationBarItem(
            icon: Icon(Icons.text_format), title: Text('Anagram')),
        BottomNavigationBarItem(
            icon: Icon(Icons.info_outline), title: Text('Isogram')),
        BottomNavigationBarItem(
            icon: Icon(Icons.timelapse), title: Text('Guess Isogram')),
      ],
      unselectedItemColor: Colors.grey,
      selectedItemColor: Colors.blue,
      currentIndex: _selectedPageIndex,
      onTap: _onPageIndexChanged,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
      ),
      body: _pages[_selectedPageIndex],
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }
}
