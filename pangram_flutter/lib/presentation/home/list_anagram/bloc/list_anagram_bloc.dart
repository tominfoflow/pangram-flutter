import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'list_anagram_event.dart';
part 'list_anagram_state.dart';

class ListAnagramBloc extends Bloc<ListAnagramEvent, ListAnagramState> {
  @override
  ListAnagramState get initialState => ListAnagramInitial();

  @override
  Stream<ListAnagramState> mapEventToState(
    ListAnagramEvent event,
  ) async* {
    // TODO: Add Logic
  }
}
