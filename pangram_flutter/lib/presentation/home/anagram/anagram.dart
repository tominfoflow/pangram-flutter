import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pangram_flutter/presentation/home/anagram/bloc/anagram_bloc.dart';

class AnagramPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AnagramPageState();
}

class AnagramPageState extends State<AnagramPage> {
  String _result = '';
  TextEditingController _controllerWord1;
  TextEditingController _controllerWord2;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _controllerWord1 = TextEditingController();
    _controllerWord2 = TextEditingController();
    super.initState();
  }

  @override
  dispose() {
    _controllerWord1.dispose();
    _controllerWord2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  _buildTextField('First Word', _controllerWord1),
                  Padding(padding: EdgeInsets.only(top: 20.0)),
                  _buildTextField('Second Word', _controllerWord2),
                ],
              )),
          Padding(padding: EdgeInsets.only(top: 20.0)),
          _buildButton(),
          _resultAnagram(),
        ],
      ),
    );
  }

  Widget _buildTextField(label, controller) {
    return TextFormField(
      controller: controller,
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
        return null;
      },
      decoration:
          InputDecoration(border: OutlineInputBorder(), labelText: label),
    );
  }

  Widget _buildButton() {
    return RaisedButton(
        child: Text('Checked'),
        color: Colors.blue,
        onPressed: () {
          if (_formKey.currentState.validate()) {
            BlocProvider.of<AnagramBloc>(context).add(AnagramEventChecked(
                _controllerWord1.text.trim(), _controllerWord2.text.trim()));
          }
        });
  }

  Widget _resultAnagram() {
    return BlocConsumer<AnagramBloc, AnagramState>(
      builder: (context, state) {
        if (state is AnagramCheckedState) {
          _result = state.result;
        }
        return _buildResultAnagram();
      },
      listener: (context, state) {
        return _buildResultAnagram();
      },
    );
  }

  Widget _buildResultAnagram() {
    return Expanded(
        child: Container(
      alignment: Alignment.center,
      child: Text(_result, style: TextStyle(fontSize: 25, color: Colors.deepPurple[900]),),
    ));
  }
}
