import 'dart:async';
import 'dart:collection';

import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:pangram_flutter/domain/usecases/anagram_usecase.dart';

part 'anagram_event.dart';
part 'anagram_state.dart';

@injectable
class AnagramBloc extends Bloc<AnagramEvent, AnagramState> {
  final AnagramCheckedUseCase anagramCheckedUseCase;

  AnagramBloc({this.anagramCheckedUseCase});

  @override
  AnagramState get initialState => AnagramInitialState();

  @override
  Stream<AnagramState> mapEventToState(
    AnagramEvent event,
  ) async* {
    if(event is AnagramEventChecked){
      Map<String, dynamic> map = HashMap();
      map['word1'] = event.word1;
      map['word2'] = event.word2;

      String result = await anagramCheckedUseCase.call(map);
      yield AnagramCheckedState(result: result);
    }
  }
}
