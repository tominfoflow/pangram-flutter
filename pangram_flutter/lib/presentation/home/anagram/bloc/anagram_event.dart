part of 'anagram_bloc.dart';

@immutable
abstract class AnagramEvent {}

@immutable
class AnagramEventInit extends AnagramEvent{}

@immutable
class AnagramEventChecked extends AnagramEvent{
  final String word1;
  final String word2;

  AnagramEventChecked(this.word1, this.word2);  
}