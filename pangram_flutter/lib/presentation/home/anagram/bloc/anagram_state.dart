part of 'anagram_bloc.dart';

@immutable
abstract class AnagramState {}

@immutable
class AnagramInitialState extends AnagramState {}

@immutable
class AnagramCheckedState extends AnagramState{
  final String result;

  AnagramCheckedState({this.result});  
}
