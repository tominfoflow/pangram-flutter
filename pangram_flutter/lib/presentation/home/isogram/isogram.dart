import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pangram_flutter/presentation/home/isogram/bloc/isogram_bloc.dart';

class IsogramPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => IsogramPageState();
}

class IsogramPageState extends State<IsogramPage> {
  TextEditingController _isogramController;

  @override
  void initState() {
    super.initState();
    _isogramController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    _isogramController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
          _buildTextForm(),
          _buildResult(),
      ],
    );
  }

  Widget _buildCheckedIsogram() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        _buildTextForm(),
        _buildResult(),
      ],
    );
  }

  Widget _buildTextForm() {
    return Container(
        margin: EdgeInsets.all(20.0),
        child: TextFormField(
          controller: _isogramController,
          onChanged: (value) {
            BlocProvider.of<IsogramBloc>(context)
                .add(IsogramEventChecked(word: value));
          },
          decoration: InputDecoration(
              border: OutlineInputBorder(), labelText: 'Input Word'),
        ));
  }

  Widget _buildResult() {
    return BlocConsumer<IsogramBloc, IsogramState>(builder: (context, state) {
      if (state is IsogramStateInitial) {
        return Container();
      }
      if (state is IsogramStateChecked) {
        if (state.result) {
          return Container(
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.check,
                  size: 50,
                  color: Colors.green,
                ),
                Text(
                  'Yes, ${_isogramController.text} is isogram!',
                  style: TextStyle(color: Colors.deepPurple[900], fontSize: 20),
                )
              ],
            ),
          );
        } else {
          return Expanded(
              child: Container(
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.close,
                  size: 50,
                  color: Colors.red,
                ),
                Text(
                  'Sorry, ${_isogramController.text} is not isogram!',
                  style: TextStyle(color: Colors.deepPurple[900], fontSize: 20),
                )
              ],
            ),
          ));
        }
      }
      return Container();
    }, listener: (context, state) {
      return Container();
    });
  }

  Widget _buildIsogramGame() {
    return Container(
        margin: EdgeInsets.all(20.0),
        alignment: Alignment.bottomRight,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildRandomIsogram(),
          ],
        ));
  }

  Widget _buildRandomIsogram() {
    return BlocConsumer<IsogramBloc, IsogramState>(
      builder: (context, state) {
        if (state is IsogramStateRandom) {}
        return Card(
          child: Text('data'),
        );
      },
      listener: (context, state) {
        return Container(
          child: Text('data'),
        );
      },
    );
  }
}
