import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:pangram_flutter/common/model/usecase.dart';
import 'package:pangram_flutter/domain/usecases/isogram_usecase.dart';

part 'isogram_event.dart';
part 'isogram_state.dart';

@injectable
class IsogramBloc extends Bloc<IsogramEvent, IsogramState> {
  final IsogramCheckedUsecase isogramCheckedUsecase;
  final IsogramRandomUsecase isogramRandomUsecase;

  IsogramBloc(this.isogramCheckedUsecase, this.isogramRandomUsecase);

  @override
  IsogramState get initialState => IsogramStateInitial();

  @override
  Stream<IsogramState> mapEventToState(
    IsogramEvent event,
  ) async* {
    if (event is IsogramEventChecked) {
      if (event.word.length > 0) {
        String result = await isogramCheckedUsecase.call(event.word);
        yield IsogramStateChecked(result == 'true' ? true : false);
      } else {
        yield IsogramStateInitial();
      }
    }

    if (event is IsogramEventRandom) {
      String result = await isogramRandomUsecase.call(NoPayload());
      yield IsogramStateRandom(jsonDecode(result));
    }
  }
}
