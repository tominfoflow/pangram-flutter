part of 'isogram_bloc.dart';

@immutable
abstract class IsogramState {}

@immutable
class IsogramStateInitial extends IsogramState {}

@immutable
class IsogramStateLoading extends IsogramState {}

@immutable
class IsogramStateChecked extends IsogramState {
  final bool result;

  IsogramStateChecked(this.result);
}

@immutable
class IsogramStateRandom extends IsogramState {
  final List<dynamic> result;

  IsogramStateRandom(this.result);
}
