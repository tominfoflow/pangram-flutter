part of 'isogram_bloc.dart';

@immutable
abstract class IsogramEvent {}

@immutable
class IsogramEventInit extends IsogramEvent {}

@immutable
class IsogramEventLoading extends IsogramEvent {}

@immutable
class IsogramEventChecked extends IsogramEvent {
  final String word;
  IsogramEventChecked({this.word});
}

@immutable
class IsogramEventRandom extends IsogramEvent {}
