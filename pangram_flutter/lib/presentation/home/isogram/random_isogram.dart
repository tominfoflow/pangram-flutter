import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pangram_flutter/presentation/home/isogram/bloc/isogram_bloc.dart';

class RandomIsogramPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RandomIsogramPageState();
}

class RandomIsogramPageState extends State<RandomIsogramPage> {
  String _randomWord = '';

  @override
  void initState() {    
    super.initState();
    BlocProvider.of<IsogramBloc>(context).add(IsogramEventRandom());
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.all(12.0),
        child: Column(
          children: <Widget>[
            BlocConsumer<IsogramBloc, IsogramState>(builder: (context, state){
              if(state is IsogramStateRandom){
                Map<String, dynamic> map = state.result[0];
                _randomWord = map['word'];
              }
              return Text(_randomWord, style: TextStyle(fontSize: 25),);
            }, listener: (context, state){
              return Container();
            },), 
            Flex(direction: Axis.horizontal, children: <Widget>[
              RaisedButton(onPressed: (){

              }), 
              RaisedButton(onPressed: (){
                
              })
            ],)
          ],
        ),
      ),
    );
  }
}
