import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/data/datasources/isogram_datasource.dart';
import 'package:pangram_flutter/domain/repositories/isogram_repository.dart';

@injectable
@lazySingleton
class IsogramRepositoryImpl extends IsogramRepository{

  final IsogramDatasource isogramDatasource;

  IsogramRepositoryImpl(this.isogramDatasource);

  @override
  Future<String> isogramChecked(String word) async {
    return await isogramDatasource.isogramChecked(word: word);
  }

  @override
  Future<String> randomIsogram() async {
    return await isogramDatasource.randomIsogram();
  }

}