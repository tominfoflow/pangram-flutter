import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/data/datasources/pangram_datasource.dart';
import 'package:pangram_flutter/domain/repositories/pangram_repository.dart';

@lazySingleton
@injectable
class PangramRepositoryImpl extends PangramRepository{
  final PangramDatasource pangramDatasource;

  PangramRepositoryImpl({this.pangramDatasource});

  @override
  Future<String> getPangramOfTheDay() async {
    return await pangramDatasource.getPangramOfTheDay();
  }

}