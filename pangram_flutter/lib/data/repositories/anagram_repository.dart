import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/data/datasources/anagram_datasource.dart';
import 'package:pangram_flutter/domain/repositories/anagram_repository.dart';

@lazySingleton
@injectable
class AnagramRepositoryImpl extends AnagramRepository{
  final AnagramDatasource anagramDatasource;

  AnagramRepositoryImpl({this.anagramDatasource});

  @override
  Future<String> anagramChecked(String word1, String word2) async{
    return await anagramDatasource.anagramChecked(word1: word1, word2: word2);
  }

}