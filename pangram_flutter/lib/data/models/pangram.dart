import 'dart:convert';

import 'package:pangram_flutter/domain/entity/pangram.dart';

class Pangram extends PangramEntity{
  Pangram({String text}) : super(text : text);

  factory Pangram.fromJson(Map<String, dynamic> json){
    return Pangram(
      text : json['text']
    );
  }

  Map<String, dynamic> toJson(){
    return {
      'text' : text
    };
  }

  String toString(){
    return jsonEncode(toJson());
  }
}