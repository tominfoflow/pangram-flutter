import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/common/network/pangram_ws_client.dart';
import 'package:pangram_flutter/data/models/pangram.dart';

@Bind.toType(PangramDatasourceImpl)
@injectable
abstract class PangramDatasource {
  Future<List<Pangram>> getAllPangram();
  Future<String> getPangramOfTheDay();
}

@lazySingleton
@injectable
class PangramDatasourceImpl extends PangramDatasource {
  static const DOMAIN = 'pangram-nodejs.herokuapp.com';
  final PangramWsClient pangramWsClient;

  PangramDatasourceImpl({this.pangramWsClient});

  @override
  Future<List<Pangram>> getAllPangram() {
    return null;
  }

  @override
  Future<String> getPangramOfTheDay() async {
    Uri uri = Uri.https(DOMAIN, 'pangram');

    final response = await pangramWsClient.get(uri);
    return response.body;
  }
}
