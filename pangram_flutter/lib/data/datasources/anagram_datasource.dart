
import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/common/network/pangram_ws_client.dart';

@Bind.toType(AnagramDatasourceImpl)
@injectable
abstract class AnagramDatasource{
  Future<String> anagramChecked({String word1, String word2});
}

@lazySingleton
@injectable
class AnagramDatasourceImpl extends AnagramDatasource{
  static const DOMAIN = 'pangram-nodejs.herokuapp.com';
  final PangramWsClient pangramWsClient;

  AnagramDatasourceImpl(this.pangramWsClient);

  @override
  Future<String> anagramChecked({String word1, String word2}) async {
    Uri uri = Uri.https(DOMAIN, 'anagram/$word1/$word2');
    final response = await pangramWsClient.get(uri);
    return response.body;
  }

}