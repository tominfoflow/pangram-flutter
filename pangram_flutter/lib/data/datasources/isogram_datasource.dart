import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/common/network/pangram_ws_client.dart';

@Bind.toType(IsogramDatasourceImpl)
@injectable
abstract class IsogramDatasource {
  Future<String> isogramChecked({String word});
  Future<String> randomIsogram();
}

@lazySingleton
@injectable
class IsogramDatasourceImpl extends IsogramDatasource {
  static const DOMAIN = 'pangram-nodejs.herokuapp.com';
  final PangramWsClient pangramWsClient;

  IsogramDatasourceImpl(this.pangramWsClient);

  @override
  Future<String> isogramChecked({String word}) async {
    Uri uri = Uri.https(DOMAIN, 'isogram/$word');
    final response = await pangramWsClient.get(uri);
    return response.body;
  }

  @override
  Future<String> randomIsogram() async {
    Uri uri = Uri.https(DOMAIN, 'isogram/random');
    final response = await pangramWsClient.get(uri);
    return response.body;
  }
}
