import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/common/model/usecase.dart';
import 'package:pangram_flutter/domain/repositories/anagram_repository.dart';

@lazySingleton
@injectable
class AnagramCheckedUseCase extends UseCase<String, Map<String, dynamic>>{
  final AnagramRepository anagramRepository;

  AnagramCheckedUseCase(this.anagramRepository);

  @override
  Future<String> call(Map<String, dynamic> payload) async{
    return await anagramRepository.anagramChecked(payload['word1'], payload['word2']);
  }
}