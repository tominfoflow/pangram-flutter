import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/common/model/usecase.dart';
import 'package:pangram_flutter/domain/repositories/isogram_repository.dart';

@lazySingleton
@injectable
class IsogramCheckedUsecase extends UseCase<String, String>{
  final IsogramRepository isogramRepository;

  IsogramCheckedUsecase(this.isogramRepository);

  @override
  Future<String> call(String payload) async {    
    return await isogramRepository.isogramChecked(payload);
  }
}

@lazySingleton
@injectable
class IsogramRandomUsecase extends UseCase<String, NoPayload>{
  final IsogramRepository isogramRepository;

  IsogramRandomUsecase(this.isogramRepository);

  @override
  Future<String> call(NoPayload payload) async {
    return await isogramRepository.randomIsogram();
  }
  
}