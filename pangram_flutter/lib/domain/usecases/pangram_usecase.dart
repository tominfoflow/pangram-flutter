import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/common/model/usecase.dart';
import 'package:pangram_flutter/domain/repositories/pangram_repository.dart';

@lazySingleton
@injectable
class PangramOfTheDayUseCase implements UseCase<String, NoPayload>{
  final PangramRepository pangramRepository;

  PangramOfTheDayUseCase({@required this.pangramRepository});

  @override
  Future<String> call(NoPayload payload) {
    return pangramRepository.getPangramOfTheDay();
  }

}