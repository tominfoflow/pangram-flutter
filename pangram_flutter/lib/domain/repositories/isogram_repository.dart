import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/data/repositories/isogram_repository.dart';

@Bind.toType(IsogramRepositoryImpl)
@injectable
abstract class IsogramRepository{
  Future<String> isogramChecked(String word);
  Future<String> randomIsogram();
}