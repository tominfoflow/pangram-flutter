import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/data/repositories/pangram_repository.dart';

@Bind.toType(PangramRepositoryImpl)
@injectable
abstract class PangramRepository {
  Future<String> getPangramOfTheDay();
}