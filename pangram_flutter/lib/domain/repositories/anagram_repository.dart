import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/data/repositories/anagram_repository.dart';

@Bind.toType(AnagramRepositoryImpl)
@injectable
abstract class AnagramRepository {
  Future<String> anagramChecked(String word1, String word2);
}