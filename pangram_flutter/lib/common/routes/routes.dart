abstract class Routes{
  static const String homePath = 'home';
  static const String home = homePath;
  static const String pangramPath = 'pangram';
  static const String pangram = '$home/$pangramPath';
  static const String anagramPath = 'anagram';
  static const String anagram = '$home/$anagramPath';
  static const String isogramPath = 'isogram';
  static const String isogram = '$home/$isogramPath';
  static const String randomIsogramPath = 'randomIsogram';
  static const String randomIsogram = '$home/$randomIsogramPath';
}