import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pangram_flutter/common/config/injector.dart';
import 'package:pangram_flutter/common/routes/routes.dart';
import 'package:pangram_flutter/presentation/home/anagram/bloc/anagram_bloc.dart';
import 'package:pangram_flutter/presentation/home/home_page.dart';
import 'package:pangram_flutter/presentation/home/isogram/bloc/isogram_bloc.dart';
import 'package:pangram_flutter/presentation/home/pangram/bloc/pangram_bloc.dart';

enum HomePageOptions{
  pangram,
  anagram,
  isogram,
  randomIsogram
}

abstract class Router {
  static Widget home = _buildHomePage(page: HomePageOptions.pangram);

  static Route<dynamic> onGenerateRoute(RouteSettings settings){
    switch(settings.name){
      case Routes.home:
        return _buildHomeRoute(settings);
      case Routes.pangram:
        return _buildPangramRoute(settings);
      case Routes.anagram:
        return _buildAnagramRoute(settings);
      case Routes.isogram:
        return _buildIsogramRoute(settings);
    }
    return null;
  }

  static Widget _buildHomePage({HomePageOptions page}){
    return MultiBlocProvider(
      providers: [
        BlocProvider<PangramBloc>(create: (BuildContext context) => getIt<PangramBloc>(),),
        BlocProvider<AnagramBloc>(create: (BuildContext context) => getIt<AnagramBloc>(),),
        BlocProvider<IsogramBloc>(create: (BuildContext context) => getIt<IsogramBloc>(),)
      ],
      child: HomePage(page: page,),
    );
  }

  static Route _buildHomeRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) => _buildHomePage(),
    );
  }

  static Route _buildPangramRoute(RouteSettings settings){
    return MaterialPageRoute(
      builder: (context) => _buildHomePage(page: HomePageOptions.pangram),
    );
  }

  static Route _buildAnagramRoute(RouteSettings settings){
    return MaterialPageRoute(
      builder: (context) => _buildHomePage(page: HomePageOptions.anagram),
    );
  }

  static Route _buildIsogramRoute(RouteSettings settings){
    return MaterialPageRoute(
      builder: (context) => _buildHomePage(page: HomePageOptions.isogram),
    );
  }

  static Route _buildRandomIsogramRoute(RouteSettings settings){
    return MaterialPageRoute(
      builder: (context) => _buildHomePage(page: HomePageOptions.randomIsogram),
    );
  }
}

