import 'dart:convert';

import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:pangram_flutter/common/error/api_error.dart';

@Bind.toType(PangramWsClientImpl)
@injectable
abstract class PangramWsClient extends BaseClient{

}

typedef _PendingRequest = Future<Response> Function();

@lazySingleton
@injectable
class PangramWsClientImpl extends PangramWsClient{
  final Client _client;

  PangramWsClientImpl(this._client);

  @override
  Future<StreamedResponse> send(BaseRequest request) {    
    return _client.send(request);
  }

    @override
  Future<Response> get(url, {Map<String, String> headers}) {
    return super.get(url, headers: headers);
  }

  @override
  Future<Response> post(url,
      {Map<String, dynamic> headers, body, Encoding encoding}) {
    return _handleResponse(() =>
        super.post(url, headers: headers, body: body, encoding: encoding));
  }

  Future<Response> _handleResponse(_PendingRequest request) async {
    Response response = await request();
    if (response.statusCode >= 200 && response.statusCode < 300)
      return response;

    throw ApiError(
      uri: response.request.url,
      code: response.statusCode,
      body: response.body,
      method: response.request.method,
    );
  }
}