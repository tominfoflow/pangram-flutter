import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';
import 'injector.iconfig.dart' as injection_config;

final getIt = GetIt.instance;
Future<void> setupInjections() async {
  getIt.registerLazySingleton(() => http.Client());
  configure();
}

@injectableInit
void configure() => injection_config.$initGetIt();