// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:pangram_flutter/common/network/pangram_ws_client.dart';
import 'package:http/src/client.dart';
import 'package:pangram_flutter/data/datasources/anagram_datasource.dart';
import 'package:pangram_flutter/data/datasources/isogram_datasource.dart';
import 'package:pangram_flutter/data/datasources/pangram_datasource.dart';
import 'package:pangram_flutter/data/repositories/anagram_repository.dart';
import 'package:pangram_flutter/data/repositories/isogram_repository.dart';
import 'package:pangram_flutter/data/repositories/pangram_repository.dart';
import 'package:pangram_flutter/domain/repositories/anagram_repository.dart';
import 'package:pangram_flutter/domain/repositories/isogram_repository.dart';
import 'package:pangram_flutter/domain/repositories/pangram_repository.dart';
import 'package:pangram_flutter/domain/usecases/anagram_usecase.dart';
import 'package:pangram_flutter/domain/usecases/isogram_usecase.dart';
import 'package:pangram_flutter/domain/usecases/pangram_usecase.dart';
import 'package:pangram_flutter/presentation/home/anagram/bloc/anagram_bloc.dart';
import 'package:pangram_flutter/presentation/home/isogram/bloc/isogram_bloc.dart';
import 'package:pangram_flutter/presentation/home/pangram/bloc/pangram_bloc.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;
void $initGetIt({String environment}) {
  getIt
    ..registerFactory<PangramWsClient>(
        () => PangramWsClientImpl(getIt<Client>()))
    ..registerLazySingleton<PangramWsClientImpl>(
        () => PangramWsClientImpl(getIt<Client>()))
    ..registerFactory<AnagramDatasource>(
        () => AnagramDatasourceImpl(getIt<PangramWsClient>()))
    ..registerLazySingleton<AnagramDatasourceImpl>(
        () => AnagramDatasourceImpl(getIt<PangramWsClient>()))
    ..registerFactory<IsogramDatasource>(
        () => IsogramDatasourceImpl(getIt<PangramWsClient>()))
    ..registerLazySingleton<IsogramDatasourceImpl>(
        () => IsogramDatasourceImpl(getIt<PangramWsClient>()))
    ..registerFactory<PangramDatasource>(
        () => PangramDatasourceImpl(pangramWsClient: getIt<PangramWsClient>()))
    ..registerLazySingleton<PangramDatasourceImpl>(
        () => PangramDatasourceImpl(pangramWsClient: getIt<PangramWsClient>()))
    ..registerLazySingleton<AnagramRepositoryImpl>(() =>
        AnagramRepositoryImpl(anagramDatasource: getIt<AnagramDatasource>()))
    ..registerLazySingleton<IsogramRepositoryImpl>(
        () => IsogramRepositoryImpl(getIt<IsogramDatasource>()))
    ..registerLazySingleton<PangramRepositoryImpl>(() =>
        PangramRepositoryImpl(pangramDatasource: getIt<PangramDatasource>()))
    ..registerFactory<AnagramRepository>(() =>
        AnagramRepositoryImpl(anagramDatasource: getIt<AnagramDatasource>()))
    ..registerFactory<IsogramRepository>(
        () => IsogramRepositoryImpl(getIt<IsogramDatasource>()))
    ..registerFactory<PangramRepository>(
        () => PangramRepositoryImpl(pangramDatasource: getIt<PangramDatasource>()))
    ..registerLazySingleton<AnagramCheckedUseCase>(() => AnagramCheckedUseCase(getIt<AnagramRepository>()))
    ..registerLazySingleton<IsogramCheckedUsecase>(() => IsogramCheckedUsecase(getIt<IsogramRepository>()))
    ..registerLazySingleton<IsogramRandomUsecase>(() => IsogramRandomUsecase(getIt<IsogramRepository>()))
    ..registerLazySingleton<PangramOfTheDayUseCase>(() => PangramOfTheDayUseCase(pangramRepository: getIt<PangramRepository>()))
    ..registerFactory<AnagramBloc>(() => AnagramBloc(anagramCheckedUseCase: getIt<AnagramCheckedUseCase>()))
    ..registerFactory<IsogramBloc>(() => IsogramBloc(getIt<IsogramCheckedUsecase>(), getIt<IsogramRandomUsecase>()))
    ..registerFactory<PangramBloc>(() => PangramBloc(pangramOfTheDayUseCase: getIt<PangramOfTheDayUseCase>()));
}
