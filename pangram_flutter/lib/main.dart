import 'package:flutter/material.dart';
import 'package:pangram_flutter/app.dart' as app;
import 'package:pangram_flutter/common/config/injector.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupInjections();
  app.main();
}